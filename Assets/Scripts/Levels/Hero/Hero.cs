﻿using UnityEngine;
using System;

public class Hero : MonoBehaviour {

    public Vector3 startPosition;

	public float walkVelocity;
	public float runVelocity;
	public StateMachine stateMachine;
    public Animator animator;
    public float rightTouchMoment;
    public Animator accelerator;
    public Animator jumper;
    public float jumperForce;
    public SoundManager soundManager;
    public GameObject energeticEffect;


    private Rigidbody2D _body;
    private TriggerChecker _crushTriggerHead;
    private TriggerChecker _crushTriggerBody;
    public TriggerChecker _leaveGroundTriggerChecker;
    private Transform _cameraContainer;
    private TriggerChecker _bordTriggerChecker;
    private TriggerChecker _pushTriggerChecker;
    public float additionalVelocity;
    bool preventAddVelReduction;

    private ParticleSystem shadowEffect;
    private ParticleSystem shadowEffect1;

    private GameStateLevel stateLevel;

    public Filters camEffects;


    public Transform spring;

    private GameObject skin;

    private float freeRotVelocity = 11;

    public bool rotating;

    protected void Awake()
    {
        animator = GameObject.Find("Hero/Skin").GetComponent<Animator>();
        skin = GameObject.Find("Hero/Skin");
        soundManager = GameObject.Find("Manager").GetComponent<SoundManager>();

        stateLevel = GameObject.Find("Manager").GetComponent<EasingManager>().GetComponent<GameStateLevel>();

        shadowEffect = GameObject.Find("Hero/ShadowBlur").GetComponent<ParticleSystem>();
        shadowEffect1 = GameObject.Find("Hero/ShadowBlur1").GetComponent<ParticleSystem>();

        energeticEffect = GameObject.Find("Hero/ParticlesFire");
        energeticEffect.SetActive(false);

        _cameraContainer = GameObject.Find("CameraContainer").transform;


        _body = GetComponent<Rigidbody2D>();

        _bordTriggerChecker = GameObject.Find("Hero/BordTrigger").GetComponent<TriggerChecker>();
        _bordTriggerChecker.enterActions += (Collider2D collider) =>
        {
            if (collider.gameObject.layer == 9)
            {
                stateMachine.SwitchState("HeroStateCrash");
                return;
            }
        };

        _pushTriggerChecker = GameObject.Find("Hero/Push").GetComponent<TriggerChecker>();
        _pushTriggerChecker.enterActions = (Collider2D collider) =>
        {
            if (collider.gameObject.layer == 14)
            {
                accelerator = collider.gameObject.GetComponent<Animator>();
                accelerator.SetTrigger("Ready");
                soundManager.PlayEffect(soundManager.acceleratorReady);
            }
            else if (collider.gameObject.layer == 15)
            {
                jumper = collider.gameObject.GetComponent<Animator>();
                jumperForce = collider.gameObject.GetComponent<JumperForce>().jumpForce;
                jumper.SetTrigger("Ready");
                soundManager.PlayEffect(soundManager.springReady);
            }
            else if (collider.gameObject.layer == 21)
            {
                jumperForce = collider.gameObject.GetComponent<JumperForce>().jumpForce;
                spring = collider.transform;
            }
            else if (collider.gameObject.layer == 22)
            {
                rotating = true;
            }
        };

        _pushTriggerChecker.exitActions = (Collider2D collider) =>
        {
            if (collider.gameObject.layer == 14)
            {
                accelerator = null;
            }
            else if (collider.gameObject.layer == 15)
            {
                jumper = null;
            }
        };

        _crushTriggerHead = GameObject.Find("Hero/Head").GetComponent<TriggerChecker>();
        _crushTriggerHead.enterActions += (Collider2D collider) =>
        {
            if (collider.gameObject.layer == 0 || collider.gameObject.layer == 8 || collider.gameObject.layer == 9) stateMachine.SwitchState("HeroStateCrashHead");
        };

        _crushTriggerBody = GameObject.Find("Hero/Belly").GetComponent<TriggerChecker>();
        _crushTriggerBody.enterActions += (Collider2D collider) =>
        {

            if(collider.gameObject.layer == 0 || collider.gameObject.layer == 8 || collider.gameObject.layer == 9) stateMachine.SwitchState("HeroStateCrashBody");
        };

        _leaveGroundTriggerChecker = GameObject.Find("Hero/LeaveGroundTrigger").GetComponent<TriggerChecker>();
        _leaveGroundTriggerChecker.exitActions = (Collider2D collider) =>
        {
            if (_leaveGroundTriggerChecker.collidersIn.Count == 0)
            {
               if(!rotating) stateMachine.SwitchState("HeroStateInAir");
               else stateMachine.SwitchState("HeroStateRotating");
            }
        };

        _leaveGroundTriggerChecker.enterActions = (Collider2D collider) =>
        {

            if (collider.gameObject.layer == 18)
            {
                additionalVelocity = 50;
                preventAddVelReduction = true;
                Statics.addVelReduser = .2f;
                stateLevel.Zoom(2, .4f, false);
                return;
            }

            if (collider.gameObject.layer == 19)
            {
                preventAddVelReduction = false;

                VelocityReduceAmount amountHolder = collider.gameObject.GetComponent<VelocityReduceAmount>();
                Statics.addVelReduser = amountHolder == null ? .2f : amountHolder.amount;

                stateLevel.ZoomBack();
                return;
            }

            if (collider.gameObject.layer == 10)
            {
                Statics.RoundWin();
                //stateMachine.SwitchState("HeroStateWin");
                EasyTwinner.Instance.TweenValue(transform, "tv_curvature", (x) => camEffects.m_TvCurvature = x, camEffects.m_TvCurvature, .7f, .4f, Easing.Linear);
                return;
            }
        };
    }

    public void Reset()
    {
        stateMachine.enabled = true;
        gameObject.SetActive(true);
        stateMachine.SwitchState("HeroStateInAir");

        shadowEffect.Pause();
        shadowEffect1.Pause();

        EasyTwinner.Instance.TweenValue(transform, "tv_curvature", (x) => camEffects.m_TvCurvature = x, camEffects.m_TvCurvature, Statics.tvCurvate, 7, Easing.Linear);

        additionalVelocity = 0;
        runVelocity = 0;
        skin.transform.localRotation = Quaternion.Euler(0, 0, 0);

        // gameObject.transform.position = new Vector3(-150, -6.5f, 0);
        gameObject.transform.position = startPosition;
        gameObject.SetActive(true);
        _body.velocity = new Vector2(runVelocity, 0);
        _body.rotation = 0;

        /*_crushTriggerBody.enterActions = null;
        _crushTriggerHead.enterActions = null;
        _crushTriggerBody.exitActions = null;
        _crushTriggerHead.exitActions = null;
        _leaveGroundTriggerChecker.enterActions = null;
        _leaveGroundTriggerChecker.exitActions = null;*/

        _crushTriggerBody.collidersIn.Clear();
        _crushTriggerHead.collidersIn.Clear();
        _leaveGroundTriggerChecker.collidersIn.Clear();

        SetAnimTrigger();
        animator.enabled = true;
        rotating = false;
    }

    public void SetAnimTrigger(string triggerName = null)
    {
        if (!animator) return;
        foreach (AnimatorControllerParameter parameter in animator.parameters)
        {
            animator.ResetTrigger(parameter.name);
        }

        if (triggerName == null) return;

        animator.SetTrigger(triggerName);
    }

    private void Clear()
    {
        _crushTriggerBody.enterActions = null;
        _crushTriggerHead.enterActions = null;
        _crushTriggerBody.exitActions = null;
        _crushTriggerHead.exitActions = null;
    }

    private void MoveCameraContainer()
    {
        /*Vector3 destination = Vector3.zero;
        if(_cameraContainer.localPosition == Vector3.zero)
        {
            float shiftAngle = Mathf.PI * 2 * Random.value;
            destination = new Vector3(_cameraContainer.localPosition.x + Mathf.Cos(shiftAngle) * .7f, _cameraContainer.localPosition.y + Mathf.Sin(shiftAngle) * .7f, _cameraContainer.localPosition.z);
        }

        Debug.Log(destination);

        _easingManager.StartEasingAction(ActionTypes.Move, EasingTypes.Linear, EasingDirection.InOut, .1f, destination, _cameraContainer.gameObject, 0, MoveCameraContainer);*/
    }

    private void Start()
    {
        MoveCameraContainer();
    }

    private void FixedUpdate()
    {

        runVelocity = Statics.currentVelocity + additionalVelocity;

        if (!Statics.isRideStarted) return;

        //skin.transform.Rotate(new Vector3(0, 0, 21));

        if (additionalVelocity > 4 && shadowEffect.isPaused)
        {
            shadowEffect.Play();
            shadowEffect1.Play();
        }
        else if (shadowEffect.isPlaying)
        {
            shadowEffect.Pause();
            shadowEffect1.Pause();
        }

        if (additionalVelocity > 0 && !preventAddVelReduction)
        {
            additionalVelocity -= Statics.addVelReduser;
        }
        else if (additionalVelocity < 0)
        {
            additionalVelocity = 0;
        }

        if (rotating) return;

        float absRotation = Mathf.Abs(_body.rotation);
        float validRotation = absRotation - 360 * Mathf.Floor(absRotation / 360);

        if (validRotation > 50) _body.rotation /= 2f;

        if (_body.velocity.x < 8 || validRotation > 55)
        {
            stateMachine.SwitchState("HeroStateCrash");
        }
    }
}