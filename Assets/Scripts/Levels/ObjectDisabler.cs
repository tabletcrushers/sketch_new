﻿using System.Collections.Generic;
using UnityEngine;

public class ObjectDisabler : MonoBehaviour
{

    Rigidbody2D bodyToCompare;
    int pauseCounter = 70;
    List<Rigidbody2D> allBodies;

    void Start()
    {
        bodyToCompare = GameObject.Find("Hero").GetComponent<Rigidbody2D>();
        allBodies = new List<Rigidbody2D>(FindObjectsOfType(typeof(Rigidbody2D)) as Rigidbody2D[]);
    }

    void Update()
    {
        if (!bodyToCompare) return;

        if(pauseCounter > 0)
        {
            pauseCounter--;
            return;
        }

        pauseCounter = 70;

        for (int i = allBodies.Count - 1; i >= 0; i--)
        {
            Rigidbody2D body = allBodies[i];

            if(body.position.x < bodyToCompare.position.x - 7)
            {
                body.gameObject.SetActive(false);
                allBodies.Remove(body);
            }
        }
    }
}
