﻿using UnityEngine.UI;

public abstract class GameStateWithCurtain : State
{
    public RawImage curtain;
    public bool fadeIn;
    public float duration = 1f;
    public float delay;

    override protected void Awake()
    {
        base.Awake();

        onEnterAction = () =>
        {
            curtain.gameObject.SetActive(true);

            if (delay < .1f)
            {
                ManageCurtain();
            }
            else
            {
                StartCoroutine(TimeUtils.Delay(delay, ManageCurtain));
            }
        };
    }

    private void ManageCurtain()
    {
        float alpha = fadeIn ? 1f : 0f;

        curtain.CrossFadeAlphaWithCallBack(alpha, duration, OnFadeFinished);
    }

    protected abstract void OnFadeFinished();
}
