﻿#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;

class ObjectQueueMaker : EditorWindow
{

    private GameObject mainInstance;
    private GameObject extremeInstance;
    private int partsAmount;
    private float offset;
    private bool xDirection;
    private Vector3 customBounds;

    GameObject parentContainer;

    [MenuItem("Window/GameObject management/Make objects sequence")]
    static void Init()
    {
        ObjectQueueMaker window = (ObjectQueueMaker)EditorWindow.GetWindow(typeof(ObjectQueueMaker));
        window.Show();
    }

    private void OnGUI()
    {
        EditorGUILayout.LabelField("CREATE DIRECTED SEQUENCE"); 

        mainInstance = (GameObject)EditorGUILayout.ObjectField("Instance", mainInstance, typeof(GameObject), false);
        extremeInstance = (GameObject)EditorGUILayout.ObjectField("Extreme part", extremeInstance, typeof(GameObject), false);
        partsAmount = EditorGUILayout.IntField("Amount of parts:", partsAmount);
        xDirection = EditorGUILayout.Toggle("X direction", xDirection);
        customBounds = EditorGUILayout.Vector3Field("Custon bounds", customBounds);
        offset = EditorGUILayout.FloatField("offset:", offset);


        if (GUILayout.Button("Create directed queue!"))
        {
            if (parentContainer != null) DestroyImmediate(parentContainer);
            parentContainer = ArrangeChildren.MakeQueue(mainInstance, partsAmount, extremeInstance, xDirection, customBounds, offset);
            parentContainer.transform.position = new Vector3(SceneView.lastActiveSceneView.pivot.x, SceneView.lastActiveSceneView.pivot.y);
            GUIUtility.ExitGUI();
        }

        if (GUILayout.Button("Fix it!"))
        {
            parentContainer = null;
            GUIUtility.ExitGUI();
        }
    }

}

#endif