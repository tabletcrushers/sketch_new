﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EasyTwinner : MonoBehaviour
{
    public static EasyTwinner Instance { get; private set; }

    private Dictionary<Transform, Dictionary<string, IEnumerator>> coroutines;

    public string PositionAlias { get; } = "position_iejfherifh$%^*#@eroifh";
    public string RotationAlias { get; } = "rotation_iejfherifh$%^*#@eroifh";
    public string ScaleAlias { get; } = "scale_iejfherifh$%^*#@eroifh";
    public string AlphaAlias { get; } = "alpha_iejfher%%^ifh$%^*#@eroifh";

    void Awake()
    {
        Instance = this;
    }

    Dictionary<string, IEnumerator> GetTransformsCoroutines(Transform target)
    {
        if(coroutines == null)
        {
            coroutines = new Dictionary<Transform, Dictionary<string, IEnumerator>>();
        }

        if (!coroutines.TryGetValue(target, out Dictionary<string, IEnumerator> transformsCoroutines))
        {
            transformsCoroutines = new Dictionary<string, IEnumerator>();
            coroutines.Add(target, transformsCoroutines);
        }

        return transformsCoroutines;
    }

    IEnumerator GetCoroutineByAlias(string coroutineAlias, Dictionary<string, IEnumerator> transformsCoroutines)
    {
        if (!transformsCoroutines.TryGetValue(coroutineAlias, out IEnumerator coroutine))
        {
            return null;
        }

        return coroutine;
    }

    void RunCoroutine(Transform target, IEnumerator coroutine, string alias)
    {
        Dictionary<string, IEnumerator> transformCoroutines = GetTransformsCoroutines(target);

        IEnumerator curentCoroutine = GetCoroutineByAlias(alias, transformCoroutines);

        if (curentCoroutine != null)
        {
            StopCoroutine(curentCoroutine);
            transformCoroutines.Remove(alias);
        }

        StartCoroutine(coroutine);
        if(alias != "") transformCoroutines.Add(alias, coroutine);
    }

    public void StopAll()
    {
        StopAllCoroutines();
        coroutines.Clear();
    }

    public void StopTween(Transform target, string alias)
    {
        Dictionary<string, IEnumerator> transformCoroutines = GetTransformsCoroutines(transform);

        IEnumerator coroutineBeStopped = GetCoroutineByAlias(alias, transformCoroutines);

        if (coroutineBeStopped != null)
        {
            StopCoroutine(coroutineBeStopped);
            transformCoroutines.Remove(alias);
        }
    }

    public void StopAllOfCertain(Transform target)
    {
        Dictionary<string, IEnumerator> transformCoroutines = GetTransformsCoroutines(target);

        foreach (IEnumerator c in transformCoroutines.Values)
        {
            StopCoroutine(c);
        }

        transformCoroutines.Clear();
    }

    public void Delay(Transform target, float time, Action action, string alias = "")
    {
        IEnumerator cr = Delayer(target, time, action);
        RunCoroutine(target, cr, alias);
    }

    private IEnumerator Delayer(Transform target, float time, Action action)
    {
        yield return new WaitForSeconds(time);

        action();
    }

    public void TweenValue(Transform target, string alias, Action<float> action, float startpos, float endpos, float seconds, Func<float, float> Easing, Action complete = null)
    {
        IEnumerator cr = ValueTwiner(target, action, startpos, endpos, seconds, Easing, alias, complete);
        RunCoroutine(target, cr, alias);
    }

    public void TweenSpriteAlpha(Transform target, float endAlpha, float seconds, Func<float, float> Easing, Action complete = null)
    {
        SpriteRenderer r = target.GetComponent<SpriteRenderer>();
        IEnumerator cr = ValueTwiner(target, (float val) =>
        {
            Color c = target.GetComponent<SpriteRenderer>().color;
            c.a = val;
            Debug.Log(c.a);
            target.GetComponent<SpriteRenderer>().color = c;
        },
        r.color.a, endAlpha, seconds, Easing, AlphaAlias, complete);

        RunCoroutine(target, cr, AlphaAlias);
    }

    public void TweenPosition(Transform target, Vector3 endpos, float seconds, Func<float, float> Easing, Action complete = null)
    {
        IEnumerator cr = TwinerV3(target, (Vector3 val) => target.localPosition = val, target.localPosition, endpos, seconds, Easing, PositionAlias, complete);
        RunCoroutine(target, cr, PositionAlias);
    }

    public void TweenScale(Transform target, Vector3 endpos, float seconds, Func<float, float> Easing, Action complete = null)
    {
        IEnumerator cr = TwinerV3(target, (Vector3 val) => target.localScale = val, target.localScale, endpos, seconds, Easing, ScaleAlias, complete);
        RunCoroutine(target, cr, ScaleAlias);
    }

    public void TweenRotation(Transform target, Vector3 endpos, float seconds, Func<float, float> Easing, Action complete = null)
    {
        IEnumerator cr = TwinerV3(target, (Vector3 val) => target.localRotation = Quaternion.Euler(val), target.rotation.eulerAngles, endpos, seconds, Easing, RotationAlias, complete);
        RunCoroutine(target, cr, RotationAlias);
    }

    public void Oscilatevalue(Transform target, string alias, Action<float> action, float amplitude = 1f, float decay = 1f, float frequence = 4f, uint stepsLim = 480, Action complete = null)
    {
        IEnumerator cr = ValueOscillator(target, alias, action, amplitude, decay, frequence, stepsLim, complete);
        RunCoroutine(target, cr, alias);
    }

    public void OscilatePosition(Transform target, Vector3 amplitudes, Vector3 decays, Vector3 frequences, uint stepsLim = 480)
    {
        Vector3 startPosition = target.localPosition;

        void processCoordinate(Vector3 v) => target.localPosition = startPosition + v;

        IEnumerator cr = Oscillator(target, PositionAlias, processCoordinate, amplitudes, decays, frequences, stepsLim);
        RunCoroutine(target, cr, PositionAlias);
    }

    public void OscilateScale(Transform target, Vector3 amplitudes, Vector3 decays, Vector3 frequences, uint stepsLim = 480)
    {
        Vector3 startPosition = target.localScale;

        void processCoordinate(Vector3 v) => target.localScale = startPosition + v;

        IEnumerator cr = Oscillator(target, ScaleAlias, processCoordinate, amplitudes, decays, frequences, stepsLim);
        RunCoroutine(target, cr, ScaleAlias);
    }

    public void OscilateRotation(Transform target, Vector3 amplitudes, Vector3 decays, Vector3 frequences, uint stepsLim = 480)
    {
        Vector3 startPosition = target.rotation.eulerAngles;

        void processCoordinate(Vector3 v) => target.rotation = Quaternion.Euler(startPosition + v);

        IEnumerator cr = Oscillator(target, RotationAlias, processCoordinate, amplitudes, decays, frequences, stepsLim);
        RunCoroutine(target, cr, RotationAlias);
    }

    private IEnumerator TwinerV3(Transform target, Action<Vector3> action, Vector3 startPos, Vector3 endpos, float seconds, Func<float, float> Easing, string alias, Action complete = null)
    {
        float t = .0f;

        while (t <= 1.0)
        {
            t += Time.deltaTime / seconds;
            float step = Easing(t);

            action(Vector3.LerpUnclamped(startPos, endpos, step));

            yield return new WaitForEndOfFrame();
        }

        action(endpos);
        GetTransformsCoroutines(target).Remove(alias);
        complete?.Invoke();
    }

    private IEnumerator ValueTwiner(Transform target, Action<float> action, float startpos, float endpos, float seconds, Func<float, float> Easing, string alias, Action complete = null)
    {
        float t = .0f;

        while (t <= 1.0)
        {
            t += Time.deltaTime / seconds;
            float step = Easing(t);
            action(Mathf.Lerp(startpos, endpos, step));
            yield return new WaitForSeconds(0);
        }

        GetTransformsCoroutines(target).Remove(alias);
        action(endpos);
        complete?.Invoke();
    }

    private IEnumerator Oscillator(Transform target, string alias, Action<Vector3> action, Vector3 amplitudes, Vector3 decays, Vector3 frequences, uint stepsLim = 480, Action complete = null)
    {
        uint steps = 0;
        float startTime = Time.time;
        float t;

        for (; ; )
        {
            t = Time.time - startTime;
            float valueX = amplitudes.x * Mathf.Sin(t * frequences.x * Mathf.PI * 2) / Mathf.Exp(t * decays.x);
            float valueY = amplitudes.y * Mathf.Sin(t * frequences.y * Mathf.PI * 2) / Mathf.Exp(t * decays.y);
            float valueZ = amplitudes.z * Mathf.Sin(t * frequences.z * Mathf.PI * 2) / Mathf.Exp(t * decays.z);

            action(new Vector3(valueX, valueY, valueZ));
            steps++;
            if (steps > stepsLim)
            {
                break;
            }
            yield return new WaitForEndOfFrame();
        }

        action(Vector3.zero);
        GetTransformsCoroutines(target).Remove(alias);
        complete?.Invoke();
    }

    private IEnumerator ValueOscillator(Transform target, string alias, Action<float> action, float amplitude = 1f, float decay = 1f, float frequence = 4f, uint stepsLim = 480, Action complete = null)
    {
        uint steps = 0;
        float startTime = Time.time;
        float t;

        for (; ; )
        {
            t = Time.time - startTime;
            float value = amplitude * Mathf.Sin(t * frequence * Mathf.PI * 2) / Mathf.Exp(t * decay);
            action(value);
            steps++;
            if (steps > stepsLim)
            {
                break;
            }
            yield return new WaitForEndOfFrame();
        }

        action(0);
        GetTransformsCoroutines(target).Remove(alias);
        complete?.Invoke();
    }

}