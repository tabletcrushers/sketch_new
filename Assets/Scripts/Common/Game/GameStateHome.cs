﻿using UnityEngine;

public class GameStateHome : GameStateWithCurtain
{
    public GameObject homeGUI;
    public GameObject gameGUI;

    protected override void Awake()
    {
        base.Awake();

        homeGUI.SetActive(true);
        gameGUI.SetActive(false);

        onEnterAction += () =>
        {
            Camera.main.transform.position = new Vector3(0, -8, Statics.camZ);
        };
    }

    override protected void OnFadeFinished()
    {
        curtain.gameObject.SetActive(false);
    }
}
