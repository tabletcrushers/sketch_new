﻿using UnityEngine;
using System.Collections.Generic;

public class GameStateLevel : GameStateWithCurtain
{
    public GameObject hero;
    public Transform target;
    public Transform follower;

    public float trembleDeltaMin = .2f;
    public float trembleDeltaMax = 1.2f;

    public Transform bgTransform;

    public FadeEffect faderOut;

    private float trembleDeltaX;
    private float trembleDeltaY;

    private float deltaX;

    private float previousCameraPositionX = 0;
    private float previousCameraPositionY = 0;

    private float heroNormalY = -8;
    private float maxYDifference = 5;

    public Rigidbody2D bord;
    public StateMachine heroStateMachine;

    private CameraContainerSwing swingCamera;

    private Hero _hs;

    private EasingManager m;

    private float zoom;
    private float camYRotation;

    public Filters camEffects;

    public UnityEngine.UI.Text totalDistanceText;
    public UnityEngine.UI.Text gottenDistanceText;

    private float startPoint;
    private float endPoint;
    private int totalDistance;

    public Texture2D fontAtlas;
    public TextAsset fontDescriptor;

    private Dictionary<string, FGlyph> font;

    override protected void OnFadeFinished()
    {
        curtain.gameObject.SetActive(false);
    }

    override protected void Awake()
    {
        base.Awake();

        swingCamera = GameObject.Find("CameraContainer").GetComponent<CameraContainerSwing>();

        font = FFontImporter.CreateFFont(fontDescriptor, fontAtlas, "Text");

        foreach(FGlyph g in font.Values)
        {
            g.GlyphObj.transform.Translate(new Vector3(10000, 0, 0));
        }



        m = GameObject.Find("Manager").GetComponent<EasingManager>();
        _hs = hero.GetComponent<Hero>();

        onEnterAction += () =>
        {
            // StopAllCoroutines();

            Statics.isRideStarted = false;
            Statics.currentVelocity = 2;
            camEffects.m_TvCurvature = 1;



            StartCoroutine(TimeUtils.Delay(4, () => { Statics.isRideStarted = true; Statics.currentVelocity = Statics.defaultVelocity; }));

            deltaX = Statics.camDeltaX;
            camYRotation = Statics.camYRotation;

            //Time.timeScale = .2f;
            Physics2D.autoSimulation = true;

            Statics.levelToDestroy = Instantiate(Resources.Load("Levels/Level" + Statics.currentLevel, typeof(GameObject)) as GameObject);

            hero.SetActive(true);
            _hs.Reset();

            Camera.main.transform.rotation = new Quaternion(0.0f, 0.1f, 0.0f, 1.0f);
            Camera.main.transform.position = new Vector3(0, -44, 2);

            zoom = 42;

            EasyTwinner.Instance.Delay(transform, 1, () =>
            {
                EasyTwinner.Instance.TweenValue(transform, "cameraZoom", (x) => zoom = x, zoom, Statics.camZ, 1, Easing.Quadratic.Out);
            });

            swingCamera.StartLevel();

            startPoint = hero.transform.position.x;
            endPoint = GameObject.Find("Level" + Statics.currentLevel + "(Clone)/Finish").transform.position.x;

            totalDistance = (int)(endPoint - startPoint);

            totalDistanceText.text = ": " + totalDistance;

            GameObject p = FFontImporter.CreatePhrase("FRESSPIN", font);

            p.transform.parent = Camera.main.transform;

            p.transform.localScale = new Vector3(28, 28, 1);

            Vector3 bounds = ArrangeChildren.CalculateLocalBounds(p);

            p.transform.localPosition = new Vector3(-bounds.x / 2, bounds.y, 100);

            p.transform.Rotate(new Vector3(0, 14, 0));

            ComeByScale cbs = p.AddComponent<ComeByScale>();
            cbs.Init();
            cbs.Show(1, 5);
        };
    }

    private void FixedUpdate()
    {
        if (Statics.currentVelocity < Statics.defaultVelocity) Statics.currentVelocity += .1f;

        gottenDistanceText.text = ": " + (int)(hero.transform.position.x - startPoint);
    }

    private void Start()
    {
        InvokeRepeating("CameraEffects", .4f, 0.04f);
    }

    private void Update()
    {
        if (!target.gameObject.activeSelf || Statics.isRoundWin)
        {
            stateMachine.SwitchState("GameStateFadeLevel");

            return;
        }

        Rigidbody2D targetBody = target.gameObject.GetComponent<Rigidbody2D>();
        previousCameraPositionX = follower.localPosition.x;
        previousCameraPositionY = follower.localPosition.y;

        float diffY = target.position.y - heroNormalY;

        float desiredYPosition = -8;

        if (Mathf.Abs(diffY) > maxYDifference) { desiredYPosition += diffY > 0 ? diffY - maxYDifference : diffY + maxYDifference - 1; }

        desiredYPosition += trembleDeltaY * 1.4f;
        float deltaStepY = (desiredYPosition - follower.localPosition.y) / 14;

        float desiredXPosition = targetBody.position.x + deltaX /*+ trembleDeltaX*/;
        float deltaStep = (desiredXPosition - follower.localPosition.x) / 1;

        follower.localPosition = new Vector3(follower.localPosition.x + deltaStep, follower.localPosition.y + deltaStepY, zoom);

        follower.rotation = Quaternion.Euler(0, camYRotation, 0);
    }

    private void CameraEffects()
    {
        trembleDeltaX = Random.Range(trembleDeltaMin, trembleDeltaMax);
        trembleDeltaY = Random.Range(trembleDeltaMin, trembleDeltaMax);
    }

    public void Zoom(float zoomK = 2, float duration = .4f, bool needBeBack = true)
    {

        EasyTwinner.Instance.TweenValue(transform, "tv_curvature", (x) => camEffects.m_TvCurvature = x, camEffects.m_TvCurvature, .7f, duration, Easing.Linear);

        System.Action<float> zb;

        if (needBeBack) zb = ZoomBack; else zb = null;

        EasyTwinner.Instance.TweenValue(transform, "zoom", (x) => zoom = x, zoom, Statics.camZ * zoomK, duration, Easing.Linear, () => { if(zb != null) zb(2); });
    }

    public void ZoomStartBack()
    {
        EasyTwinner.Instance.TweenValue(transform, "zoom", (x) => zoom = x, zoom, Statics.camZ, 1, Easing.Quintic.Out);
    }

    public void ZoomBack(float duration = 2)
    {
        EasyTwinner.Instance.TweenValue(transform, "zoom", (x) => zoom = x, zoom, Statics.camZ, duration, Easing.Quintic.Out);
        EasyTwinner.Instance.TweenValue(transform, "tv_curvature", (x) => camEffects.m_TvCurvature = x, camEffects.m_TvCurvature, Statics.tvCurvate, duration, Easing.Linear);
    }
}
