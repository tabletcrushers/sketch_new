﻿using UnityEngine;
using System.Collections;

public class PlayCollisionEffect : MonoBehaviour
{
    protected SoundManager soundManager;
    private bool soundCanBePlayed = false;
    private IEnumerator coroutine;
    private GameObject hero;

    private void Start()
    {
        soundManager = GameObject.Find("Manager").GetComponent<SoundManager>();
        hero = GameObject.Find("Hero");
        coroutine = AllowSoundToBePlayed();
        StartCoroutine(coroutine);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        float distance = Mathf.Abs(collision.gameObject.transform.position.x - hero.transform.position.x);
        if (distance > Statics.maxActiveDistance) return;
        if (!soundCanBePlayed) return;
        PlayEffect();
    }

    private IEnumerator AllowSoundToBePlayed()
    {
        yield return new WaitForSeconds(2);

        soundCanBePlayed = true;
    }

    protected virtual void PlayEffect()
    {
        
    }
}
