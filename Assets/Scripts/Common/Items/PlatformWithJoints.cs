﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformWithJoints : MonoBehaviour
{
    private HingeJoint2D jointH;
    public float fallDelay;
    private Rigidbody2D body;

    private bool fired;

    void Start()
    {
        body = GetComponent<Rigidbody2D>();
        jointH = GetComponent<HingeJoint2D>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (fired || collision.gameObject.layer != 11) return;

        fired = true;

        body.bodyType = RigidbodyType2D.Dynamic;
        body.gravityScale *= 1.4f;

        EasyTwinner.Instance.Delay(transform, .2f, () => body.bodyType = RigidbodyType2D.Static);

        EasyTwinner.Instance.Delay(transform, fallDelay, () => body.bodyType = RigidbodyType2D.Dynamic);
        EasyTwinner.Instance.Delay(transform, fallDelay + .2f, () => jointH.enabled = false);

        enabled = false;
    }
}
