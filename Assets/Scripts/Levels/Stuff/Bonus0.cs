﻿using UnityEngine;

public class Bonus0 : MonoBehaviour
{
    Hero heroScript;

    private void Start()
    {
        heroScript = GameObject.Find("Hero").GetComponent<Hero>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            heroScript.energeticEffect.SetActive(true);
            gameObject.SetActive(false);
        }
    }
}
