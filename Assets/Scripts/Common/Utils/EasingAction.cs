﻿using UnityEngine;
using UnityEngine.Events;
using System;
using System.Reflection;

[System.Serializable]
public enum EasingTypes
{
    //Linear,
    Quadratic,
    Cubic,
    Quartic,
    Quintic,
    Sinusoidal,
    Exponential,
    Circular,
    Elastic,
    Back,
    Bounce,
    Linear
}

public enum EasingDirection
{
    In,
    Out,
    InOut
}


public class EasingAction
{
    public bool finished;
    public GameObject affectedObject;
    private float startValue;
    private float deltaValue;
    private float totalTime;
    private float timeSpent;
    private UnityAction FinishCallback;
    private uint delayForFrames = 0;
    private Type[] easingTypes;

    private string[] EasingTypesString = new string[] 
    {
       // "Linear",
        "Quadratic",
        "Cubic",
        "Quartic",
        "Quintic",
        "Sinusoidal",
        "Exponential",
        "Circular",
        "Elastic",
        "Back",
        "Bounce"
    };

    private string[] EasingDirectionString = new string[]
    {
        "In",
        "Out",
        "InOut"
    };

    public EasingAction(EasingTypes easingType, EasingDirection easingDirection, float totalTime, float startValue, float finishValue, uint delayForFrames = 0)
    {
        this.totalTime = totalTime;
        this.startValue = startValue;
        this.deltaValue = finishValue - startValue;
        this.delayForFrames = delayForFrames;

        if (finishValue == startValue)
        {
            this.finished = true;
            return;
        }

        EasingFunction = getEasing(easingType, easingDirection);
    }

    private MethodInfo EasingFunction;

    private MethodInfo getEasing(EasingTypes easingType, EasingDirection easingDirection)
    {
        Type easingT = Type.GetType("Esing." + EasingTypesString[(int)easingType]);

        if (easingTypes == null)
        {
            easingTypes = typeof(Easing).GetNestedTypes();
        }
        //Type easingT = Type.GetType( Type.GetType("Esing." + EasingTypesString[(int)easingType]);
        if (easingType != EasingTypes.Linear)
        {
            return easingTypes[(int)easingType].GetMethod(EasingDirectionString[(int)easingDirection], BindingFlags.Static | BindingFlags.Public);
        }
        else
        {
            return typeof(Easing).GetMethod("Linear");
        }
    }



    public void SetFinishCallback(UnityAction callback) {
        this.FinishCallback = callback;
    }

    public float Update()
    {
        if (this.finished) return this.startValue + this.deltaValue;

        if (delayForFrames != 0) 
        {
            delayForFrames--;
            return this.startValue;
        }

        float step = this.timeSpent / this.totalTime;

        if (this.timeSpent > this.totalTime) 
        {
            this.finished = true;
            step = 1;
            if (this.FinishCallback != null) this.FinishCallback.Invoke();
        }

        this.timeSpent += Time.deltaTime;
        return this.startValue + this.deltaValue * (float)EasingFunction.Invoke(null, new object[] { step });
    }
}
