﻿using UnityEngine;

public class Pause : MonoBehaviour
{
    public StateMachine gameStateMachine;
    public PauseEffects pauseEffects;
    public GameObject pauseButton;
    public Animator heroA;

    public void OnPause()
	{
        Physics2D.autoSimulation = false;
        //Time.timeScale = 0;
        heroA.enabled = false;
        pauseEffects.Show();
        pauseButton.SetActive(false);
	}
}
