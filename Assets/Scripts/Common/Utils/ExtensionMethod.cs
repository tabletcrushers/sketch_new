﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public static class ExtensionMethod
{
    public static void CrossFadeAlphaWithCallBack(this RawImage img, float alpha, float duration, System.Action callback = null)
    {
        MonoBehaviour mnbhvr = img.GetComponent<MonoBehaviour>();
        mnbhvr.StartCoroutine(CrossFadeAlphaCOR(img, alpha, duration, callback));
    }

    private static IEnumerator CrossFadeAlphaCOR(RawImage img, float alpha, float duration, System.Action callback)
    {
        Color currentColor = img.color;

        Color visibleColor = img.color;
        visibleColor.a = alpha;


        float counter = 0;

        while (counter < duration)
        {
            counter += Time.deltaTime;
            img.color = Color.Lerp(currentColor, visibleColor, counter / duration);
            yield return null;
        }

        callback?.Invoke();
    }


    private static System.Random rng = new System.Random();

    public static void Shuffle<T>(this IList<T> list)
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }
}