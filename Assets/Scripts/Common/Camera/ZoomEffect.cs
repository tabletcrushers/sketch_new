﻿using UnityEngine;

public class ZoomEffect : MonoBehaviour
{
    private EasingManager m;

    public float ZoomEffectmTo;

    public GameObject camContainer;

    public Transform hero;

    private GameStateLevel stateLevel;

    void Awake()
    {
        m = GameObject.Find("Manager").GetComponent<EasingManager>();
        stateLevel = m.GetComponent<GameStateLevel>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        /*Debug.Log("ENTER - " + other.gameObject.layer);
        if (other.gameObject.layer != 13)
        {
            return;
        }

        Transform tempTrans = Camera.main.transform;

        tempTrans.LookAt(hero);

        Camera.main.transform.LookAt(hero);

        m.StartEasingAction(ActionTypes.Move, EasingTypes.Quadratic, EasingDirection.In, .4f, new Vector3(0, 0, ZoomEffectmTo), Camera.main.gameObject.transform.parent.gameObject, 0, ZoomBack);*/

        stateLevel.Zoom();
    }

    private void ZoomBack()
    {
        m.StartEasingAction(ActionTypes.Move, EasingTypes.Quadratic, EasingDirection.Out, .7f, new Vector3(0, 0, 0), Camera.main.gameObject.transform.parent.gameObject);
        Debug.Log("ENTER1111");
    }
}
