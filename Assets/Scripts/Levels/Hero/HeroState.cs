﻿using UnityEngine;

public abstract class HeroState : State
{
	protected Hero _hero;
	protected TouchControlsManager _touchControls;
    protected Rigidbody2D _body;
    protected TriggerChecker _bordTriggerChecker;
    protected CameraContainerSwing swingCamera;


    override protected void Awake()
	{
		base.Awake();

        _hero = GetComponent<Hero>();
        _touchControls = GameObject.Find("Manager").GetComponent<TouchControlsManager>();
        _body = GetComponent<Rigidbody2D>();
        _bordTriggerChecker = GameObject.Find("Hero/BordTrigger").GetComponent<TriggerChecker>();
        swingCamera = GameObject.Find("CameraContainer").GetComponent<CameraContainerSwing>();
    }
}
