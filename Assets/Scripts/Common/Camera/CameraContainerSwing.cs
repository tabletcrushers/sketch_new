﻿using UnityEngine;
using System.Collections;

public class CameraContainerSwing : MonoBehaviour
{
    private float camSwingY;
    private float camSwingX;
    private IEnumerator camSwingYCoroutine;
    private IEnumerator camSwingXCoroutine;
    private IEnumerator camRotationYCoroutine;
    private IEnumerator camRotationZCoroutine;
    private float rotationY;
    private float rotationZ;

    void Update()
    {
        //gameObject.transform.position = new Vector3(camSwingX, camSwingY, 0);
        //gameObject.transform.rotation = Quaternion.Euler(0, rotationY, rotationZ);
    }

    public void Landing()
    {
        /*if (camSwingYCoroutine != null) StopCoroutine(camSwingYCoroutine);
        camSwingYCoroutine = Actuate.Oscillation((x) => camSwingY = x, 2.8f, 14f, 7f, 350);
        StartCoroutine(camSwingYCoroutine);*/

        EasyTwinner.Instance.OscilatePosition(transform, new Vector3(0, 3, 0), new Vector3(0, 1, 0), new Vector3(0, 2, 0), 700);
    }

    public void SoftLanding()
    {
        EasyTwinner.Instance.OscilatePosition(transform, new Vector3(0, 1.4f, 0), new Vector3(0, 1, 0), new Vector3(0, 2, 0), 700);
    }

    public void Crush()
    {
        /*if (camSwingYCoroutine != null) StopCoroutine(camSwingYCoroutine);
        if (camSwingXCoroutine != null) StopCoroutine(camSwingXCoroutine);
        if (camRotationYCoroutine != null) StopCoroutine(camRotationYCoroutine);
        if (camRotationYCoroutine != null) StopCoroutine(camRotationZCoroutine);
        camSwingYCoroutine = Actuate.Oscillation((x) => camSwingY = x, 2.8f, 4f, 4f, 350);
        camSwingXCoroutine = Actuate.Oscillation((x) => camSwingX = x, 1f, 4f, 11f, 350);
        //camRotationYCoroutine = Actuate.Oscillation((x) => rotationY = x, .7f, 4f, 4f, 350);
        //camRotationZCoroutine = Actuate.Oscillation((x) => rotationZ = x, .7f, 4f, 4f, 350);
        StartCoroutine(camSwingYCoroutine);
        StartCoroutine(camSwingXCoroutine);
        //StartCoroutine(camRotationYCoroutine);
        //StartCoroutine(camRotationZCoroutine);*/

        //EasyTwinner.Instance.OscilatePosition(transform, new Vector3(2.8f, 1, 0), new Vector3(2.8f, 2, 0), new Vector3(2.8f, 7, 0));
        EasyTwinner.Instance.OscilatePosition(transform, new Vector3(1, 4, 0), new Vector3(4, 3, 0), new Vector3(14, 4, 0), 700);
    }

    public void StartLevel()
    {
        /*if (camSwingYCoroutine != null) StopCoroutine(camSwingYCoroutine);
        camSwingYCoroutine = Actuate.Oscillation((x) => camSwingY = x, 14f, 1f, 1f, 1400);
        StartCoroutine(camSwingYCoroutine);*/

        EasyTwinner.Instance.TweenPosition(transform, new Vector3(0, 0, 0), 1, Easing.Cubic.Out);
    }

    private void StopCr(IEnumerator cr)
    {
        if (cr != null) StopCoroutine(cr);
    }
}
