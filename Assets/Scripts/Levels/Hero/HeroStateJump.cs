﻿using UnityEngine;

public class HeroStateJump : HeroState
{
    private System.Random random;

    override protected void Awake()
	{
		base.Awake();

        random = new System.Random();

        onEnterAction = () => 
		{
            //_hero.stateMachine.SwitchState("HeroStateInAir");
            _body.gravityScale = 1;
			//if (previousState == "HeroStateRun") 
            {
                //_body.velocity = new Vector2(_hero.runVelocity, 0);
                _body.AddForce(new Vector2(0, 282));
                if(!_hero.rotating)
                {
                    string jumpID = random.Next(0, 2).ToString();
                    _hero.SetAnimTrigger("Jump" + jumpID);
                }



                if (_hero.spring != null)
                {
                    _body.AddForce(new Vector2(0, _hero.jumperForce));
                    _hero.jumperForce = 0;
                    _hero.spring.GetComponent<Collider2D>().enabled = false;
                    EasyTwinner.Instance.OscilateRotation(_hero.spring.parent, new Vector3(0, 0, 21), new Vector3(0, 0, 2), new Vector3(0, 0, 21));
                    _hero.spring = null;
                }
            }
		};
	}
}
