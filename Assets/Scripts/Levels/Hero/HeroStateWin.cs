﻿using UnityEngine;

public class HeroStateWin : HeroState
{
    override protected void Awake()
    {
        base.Awake();

        onEnterAction = () =>
        {
            Debug.Log("!!QIN!!!");
            _hero.soundManager.PlayEffect(_hero.soundManager.win);
            _hero.soundManager.decreaseSound = true;
        };
    }

    protected void FixedUpdate()
    {
        _body.velocity = new Vector2(_hero.runVelocity, _body.velocity.y);
    }
}
