﻿using UnityEngine;


public class GameStateFadeToHome : GameStateWithCurtain
{
    public GameObject homeGUI;
    public GameObject gameGUI;
    public GameObject homeBg;
    public GameObject gameBg;

    public GameObject hero;

    override protected void OnFadeFinished()
    {
        GameObject levelToDestroy = Statics.levelToDestroy;

        if (levelToDestroy)
        {
            Destroy(levelToDestroy);
        }

        hero.SetActive(false);

        homeGUI.SetActive(true);
        homeBg.SetActive(true);
        gameGUI.SetActive(false);
        gameBg.SetActive(false);
        stateMachine.SwitchState("GameStateHome");
    }
}
