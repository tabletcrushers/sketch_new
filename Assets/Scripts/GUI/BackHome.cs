﻿using UnityEngine;

public class BackHome : MonoBehaviour
{
    public PauseEffects pause;
    private StateMachine gameStateMachine;

    private void Start()
    {
        gameStateMachine = GameObject.Find("Manager").GetComponent<StateMachine>();
    }

    public void OnMouseDown()
    {
        Statics.backHome = true;
        gameStateMachine.SwitchState("GameStateFadeToHome");

        pause.Hide();
    }
}
