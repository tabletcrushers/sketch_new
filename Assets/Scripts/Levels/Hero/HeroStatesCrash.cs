﻿using UnityEngine;

public class HeroStatesCrash : HeroState
{
    public Transform boarderPrefab;
    public Transform boardPrefab;
    public Transform wheelPrefab;

    protected Transform boarder;
    protected Transform board;

    readonly Transform boomEffect;

    //Transform parentTransform;

    override protected void Awake()
    {
        base.Awake();

        swingCamera = GameObject.Find("CameraContainer").GetComponent<CameraContainerSwing>();

        onEnterAction = () =>
        {
            if (!_body.gameObject.activeSelf) return;

            EasyTwinner.Instance.StopAll();

            stateMachine.enabled = false;
            _body.gameObject.SetActive(false);

            InstantiateCrashPrefabs();
            if(board) ApplyCrashEffects();
            if (_hero.energeticEffect.activeSelf)
            {
                _hero.energeticEffect.SetActive(false);
            }
        };
    }

    void InstantiateCrashPrefabs() 
    {
        Vector3 crashPosition = _body.gameObject.transform.position;
        Transform parentTransform = GameObject.Find("Level" + Statics.currentLevel + "(Clone)").transform;
        boarder = Instantiate(boarderPrefab, new Vector3(crashPosition.x, crashPosition.y + 0/*2.4f*/, 0), Quaternion.identity);
        boarder.parent = parentTransform;
        board = Instantiate(boardPrefab, new Vector3(crashPosition.x, crashPosition.y, 0), Quaternion.identity, parentTransform);

        Instantiate(Resources.Load("Boom", typeof(GameObject)) as GameObject, new Vector3(crashPosition.x, crashPosition.y, 0), Quaternion.identity, parentTransform);

        for (int i = 0; i < 4; i++)
        {
            Transform wheel = Instantiate(Resources.Load("Wheel", typeof(GameObject)) as GameObject, new Vector3(crashPosition.x, crashPosition.y, 0), Quaternion.identity, parentTransform).transform;
            float a = Random.value * Mathf.PI;
            float force = 420 + Random.value * 420;
            Rigidbody2D wheelBody = wheel.GetComponent<Rigidbody2D>();
            wheelBody.AddForce(new Vector2(force * Mathf.Cos(a), force * Mathf.Sin(a)));
            wheelBody.angularVelocity = -350 + 700 * Random.value;
            wheelBody.gravityScale *= 2;
        }
    }

    virtual protected void ApplyCrashEffects() 
    {
        Rigidbody2D boarderBody = boarder.GetComponent<Rigidbody2D>();
        boarderBody.rotation = -45;
        boarderBody.velocity = new Vector2(_hero.runVelocity, _body.velocity.y + 7);
        boarderBody.angularVelocity = -210;
        board.GetComponent<Rigidbody2D>().velocity = new Vector2(-_hero.runVelocity / 2, _body.velocity.y + 7);
        board.GetComponent<Rigidbody2D>().angularVelocity = 2000 * Random.value;

        swingCamera.Crush();
    }
}
