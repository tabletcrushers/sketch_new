﻿using UnityEngine;


public class GameStateFadeHome : GameStateWithCurtain
{
    public GameObject homeGUI;
    public GameObject homeBg;
    public GameObject gameGUI;
    public GameObject gameBg;
    public GameObject pauseButton;


    override protected void OnFadeFinished()
    {
        homeGUI.SetActive(false);
        homeBg.SetActive(false);
        gameBg.SetActive(true);
        gameGUI.SetActive(true);
        pauseButton.SetActive(true);
        stateMachine.SwitchState("GameStateLevel");
    }
}
