﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroStateRotating : HeroState
{
    GameObject skin;
    private float rotVelocity;
    private ParticleSystem landingEffect;

    protected override void Awake()
    {
        base.Awake();
        skin = GameObject.Find("Hero/Skin");
        landingEffect = GameObject.Find("Hero/ParticlesLanding").GetComponent<ParticleSystem>();
        landingEffect.Stop();

        onEnterAction = () =>
        {
            if (previousState != "HeroStateJump")
            {
                _hero.stateMachine.SwitchState("HeroStateInAir");
                return;
            }

            rotVelocity = 280;
            _touchControls.AddCallback(TouchTypes.RightTouchBegan, OnRightTouchBegan);

            _hero.SetAnimTrigger("Rotating");

            _bordTriggerChecker.enterActions += AdditionalGroundTriggerAction;
        };

        onExitAction = () =>
        {
            _touchControls.RemoveCallback(TouchTypes.RightTouchBegan, OnRightTouchBegan);
            _bordTriggerChecker.enterActions -= AdditionalGroundTriggerAction;
        };


    }

    private float ProcessRotation(float angle)
    {
        while (angle <= -180) angle += 360;
        while (angle > 180) angle -= 360;

        return angle;
    }

    private void AdditionalGroundTriggerAction(Collider2D c)
    {
        float validRotation = ProcessRotation(_body.rotation);

        if (validRotation > 55 || validRotation < -18)
        {
            Debug.Log(validRotation);
            stateMachine.SwitchState("HeroStateCrash");
            return;
        }

        _body.rotation = 0;

        if (_body.velocity.y < -3.5f)
        {
            landingEffect.Play();
            swingCamera.Landing();
        }
        else if (_body.velocity.y < -2.5f)
        {
            swingCamera.SoftLanding();
        }

        _body.velocity = new Vector2(_hero.runVelocity, 0);

        stateMachine.SwitchState("HeroStateRun");
        _hero.SetAnimTrigger("Landing0");
        _hero.rotating = false;
    }

    private void OnRightTouchBegan(Vector2 touchPosition)
    {
        rotVelocity = 70;
        _hero.SetAnimTrigger("RotatingLanding");
    }

    private void FixedUpdate()
    {
        if (_hero.additionalVelocity > 0) _body.velocity = new Vector2(_hero.runVelocity, _body.velocity.y);

        //_body.rotation /= 1.1f;
        //skin.transform.Rotate(new Vector3(0, 0, rotVelocity));

        _body.angularVelocity = rotVelocity;
    }
}