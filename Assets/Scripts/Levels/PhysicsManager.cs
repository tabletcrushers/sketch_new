﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsManager : MonoBehaviour
{
    List<Transform> dynamicBodies = new List<Transform>();
    Transform hero;

    public void Init()
    {
        hero = GameObject.Find("Hero").transform;
        Transform[] all = GameObject.FindObjectsOfType(typeof(Transform)) as Transform[];
        foreach(Transform t in all)
        {
            Rigidbody2D b = t.GetComponent<Rigidbody2D>();
            if(b && b.bodyType == RigidbodyType2D.Dynamic && t != hero)
            {
                dynamicBodies.Add(t);
                if (!CheckActiveArea(t)) t.gameObject.SetActive(false);
            }
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        foreach(Transform t in dynamicBodies)
        {
            if(CheckActiveArea(t)) t.gameObject.SetActive(true);
            //else if (t.gameObject.activeSelf) t.gameObject.SetActive(false);
        }
    }

    private bool CheckActiveArea(Transform t)
    {
        float distance = Mathf.Abs(t.position.x - hero.position.x);
        if (distance < Statics.maxActiveDistance + 70 && !t.gameObject.activeSelf) return true;
        return false;
    }
}
