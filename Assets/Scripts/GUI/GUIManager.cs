﻿using UnityEngine;

public class GUIManager : MonoBehaviour
{

    public GameObject hero;
    public GameObject GameGUI;
    public GameObject HomeGUI;
    public GameObject Curtain;
    public StateMachine gameStateMachine;

    public void LoadLevel()
    {  
        GameObject n = Instantiate(Resources.Load("Levels/Level0", typeof(GameObject)) as GameObject);
        n.transform.position = new Vector3();

        hero.SetActive(true);
    }

    public void OnStartGame()
    {
        gameStateMachine.SwitchState("GameStateFadeHome");
    }
}
