﻿using UnityEngine;
using UnityEngine.Events;

public class StateMachine : MonoBehaviour
{

    public string currentStateName;

	public void Pause()
	{
        getState(currentStateName).enabled = false;
	}

	public void SwitchState(string name)
    {

		if(currentStateName == name)
		{
			return;
		}

        State currentState = getState(currentStateName);

        State newState = getState(name);

        if (newState != null)
        {
            TryInvoke(currentState.onExitAction);
            currentState.enabled = false;
			
			newState.enabled = true;
			newState.previousState = currentStateName;
			currentStateName = name;
			TryInvoke(newState.onEnterAction);

			return;
        }

		throw new System.Exception("there are no states named " + name);
    }

    private void Start()
    {
        foreach (State state in GetComponents<State>())
        {
            if (state.GetType().Name == currentStateName)
            {
                state.enabled = true;
            }
            else
            {
                state.enabled = false;
            }
        }

		TryInvoke(getState(currentStateName).onEnterAction);

		//getState(currentStateName).enabled = false;
	}

    private State getState(string name)
    {
        return gameObject.GetComponent(name) as State;
    }

    private void TryInvoke(UnityAction action)
    {
		if (action != null)
        {
            action.Invoke();
        }
    }
}
