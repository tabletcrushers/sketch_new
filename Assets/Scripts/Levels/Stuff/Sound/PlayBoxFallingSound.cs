﻿public class PlayBoxFallingSound : PlayCollisionEffect
{
    override protected void PlayEffect()
	{
        soundManager.PlayBoxFalling();
	}
}
