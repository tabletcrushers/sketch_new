﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spring : MonoBehaviour
{
    private Hero h;

    private void Start()
    {
        h = GameObject.Find("Hero").GetComponent<Hero>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer != 11) return;

        h.spring = transform.parent;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer != 11) return;

        h.spring = null;
    }
}
