﻿using UnityEngine;

public class Click : MonoBehaviour
{
    EasingManager m;
    public float scaleTo = 3;

    void Start()
    {
        m = GameObject.Find("Manager").GetComponent<EasingManager>();
        m.StartEasingAction(ActionTypes.Scale, EasingTypes.Cubic, EasingDirection.Out, .4f, new Vector3(scaleTo, scaleTo, 1), gameObject);
        m.EaseAlpha(EasingTypes.Linear, EasingDirection.In, .4f, 0, gameObject);
    }
}
