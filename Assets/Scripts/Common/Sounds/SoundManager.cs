﻿using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public AudioClip landingRegular;
    public AudioClip landingHard;
    public AudioClip headCrash;
    public AudioClip bodyCrash;
    public AudioClip regularCrash;
    public AudioClip acceleratorReady;
    public AudioClip acceleratorFire;
    public AudioClip springReady;
    public AudioClip springFire;
    public AudioClip win;
    public bool decreaseSound = false;

    public AudioSource bgAudioSource;

    private AudioSource heroSource;
    private AudioSource guiSource;

    private AudioSource[] collidersAudioSources;
    private AudioSource[] effectsAudioSources;

    private AudioClip bg;
    private AudioClip startEffect;

    private AudioClip[] breaksThroughWall;
    private AudioClip[] boxFallingSounds;
    private AudioClip[] steelFallingSounds;
    private AudioClip[] jumpWhoops;
    private AudioClip[] crashWhoops;

    public void PlayCollisionSound(AudioClip sound)
    {
        PlayMultiSourceSound(collidersAudioSources, sound);
    }

    public void PlayHeroSound(AudioClip sound)
    {
        heroSource.clip = sound;
        heroSource.Play();
    }

    public void PlayHeroJumpWoop()
    {
        PlayHeroSound(jumpWhoops[Random.Range(0, 2)]);
    }

    public void PlayHeroCrashWoop()
    {
        PlayHeroSound(crashWhoops[Random.Range(0, 2)]);
    }

    public void PlayEffect(AudioClip sound)
    {
        PlayMultiSourceSound(effectsAudioSources, sound);
    }

    public void PlayBrakeWallEffect()
    {
        PlayEffect(breaksThroughWall[Random.Range(0, 2)]);
    }

    public void PlayBoxFalling()
    {
        PlayCollisionSoundRandomly(boxFallingSounds);
    }

    public void PlaySteelFalling()
    {
        PlayCollisionSoundRandomly(steelFallingSounds);
    }

    public void PlayGUI(AudioClip sound)
    {
        guiSource.clip = sound;
        guiSource.Play();
    }

	private void Update()
	{
        if (!decreaseSound) return;
        bgAudioSource.volume -= .007f;
	}

	private void Awake()
    {

        heroSource = GameObject.Find("Manager/AudioSources/Hero").GetComponent<AudioSource>();
        guiSource = GameObject.Find("Manager/AudioSources/GUI").GetComponent<AudioSource>();

        collidersAudioSources = new AudioSource[2]
        {
            GameObject.Find("Manager/AudioSources/Colliders0").GetComponent<AudioSource>(),
            GameObject.Find("Manager/AudioSources/Colliders1").GetComponent<AudioSource>()
        };

        effectsAudioSources = new AudioSource[2]
        {
            GameObject.Find("Manager/AudioSources/Effects0").GetComponent<AudioSource>(),
            GameObject.Find("Manager/AudioSources/Effects1").GetComponent<AudioSource>()
        };

        win = Resources.Load<AudioClip>("Sounds/Fire");
        landingRegular = Resources.Load<AudioClip>("Sounds/mouse_click");
        landingHard = Resources.Load<AudioClip>("Sounds/mouse_click");
        headCrash = Resources.Load<AudioClip>("Sounds/mouse_click");
        bodyCrash = Resources.Load<AudioClip>("Sounds/mouse_click");
        regularCrash = Resources.Load<AudioClip>("Sounds/mouse_click");
        acceleratorReady = Resources.Load<AudioClip>("Sounds/Ready");
        acceleratorFire = Resources.Load<AudioClip>("Sounds/Fire");
        springReady = Resources.Load<AudioClip>("Sounds/Ready");
        springFire = Resources.Load<AudioClip>("Sounds/Fire");
        bg = Resources.Load<AudioClip>("Sounds/bg");
        startEffect = Resources.Load<AudioClip>("Sounds/Fire");

        PlayEffect(startEffect);

        bgAudioSource = GameObject.Find("Manager/AudioSources/Background").GetComponent<AudioSource>();
        bgAudioSource.clip = bg;
        bgAudioSource.loop = true;
        bgAudioSource.Play();

        breaksThroughWall = new AudioClip[3]
        {
            Resources.Load<AudioClip>("Sounds/brakeSteelObstacle"),
            Resources.Load<AudioClip>("Sounds/brakeSteelObstacle"),
            Resources.Load<AudioClip>("Sounds/brakeSteelObstacle")
        };

        boxFallingSounds = new AudioClip[3]
        {
            Resources.Load<AudioClip>("Sounds/mouse_click"),
            Resources.Load<AudioClip>("Sounds/mouse_click"),
            Resources.Load<AudioClip>("Sounds/mouse_click")
        };

        steelFallingSounds = new AudioClip[3]
        {
            Resources.Load<AudioClip>("Sounds/mouse_click"),
            Resources.Load<AudioClip>("Sounds/mouse_click"),
            Resources.Load<AudioClip>("Sounds/mouse_click")
        };

        jumpWhoops = new AudioClip[3]
        {
            Resources.Load<AudioClip>("Sounds/mouse_click"),
            Resources.Load<AudioClip>("Sounds/mouse_click"),
            Resources.Load<AudioClip>("Sounds/mouse_click")
        };

        crashWhoops = new AudioClip[3]
        {
            Resources.Load<AudioClip>("Sounds/mouse_click"),
            Resources.Load<AudioClip>("Sounds/mouse_click"),
            Resources.Load<AudioClip>("Sounds/mouse_click")
        };

        decreaseSound = false;
        bgAudioSource.volume = 1;
    }

    private void PlayMultiSourceSound(AudioSource[] sources, AudioClip sound)
    {
        AudioSource audioSource = null;
        foreach (AudioSource source in sources)
        {
            if (!source.isPlaying)
            {
                audioSource = source;
                break;
            }
        }
        if (audioSource)
        {
            audioSource.clip = sound;
            audioSource.Play();
        }
    }

    private void PlayCollisionSoundRandomly(AudioClip[] sounds)
    {
        PlayCollisionSound(sounds[Random.Range(0, 2)]);
    }
}
