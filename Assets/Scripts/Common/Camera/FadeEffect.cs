﻿using UnityEngine;
using UnityEngine.Events;
 
public class FadeEffect : MonoBehaviour
{
    public AnimationCurve fadeCurve;
    public UnityAction onEndCallback;
    public float startAlpha;
 

    private float _alpha;
    private Texture2D _texture;
    private float _time;
 
    public void OnGUI()
    {
        if (_texture == null) _texture = new Texture2D(1, 1);
 
        _texture.SetPixel(0, 0, new Color(0, 0, 0, _alpha));
        _texture.Apply();
 
        _time += Time.deltaTime / 2;
        _alpha = fadeCurve.Evaluate(_time);

        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), _texture);

        if (_alpha <= 0 || _alpha >= 1)
        {
            if (onEndCallback != null)
            {
                onEndCallback.Invoke();
                onEndCallback = null;
            }
            if (_alpha <= 0) StartCoroutine(TimeUtils.DelayFrames(1, () => { enabled = false; }));
        }
    }

    private void Start()
    {
        _alpha = startAlpha;
        _time = 0;
    }
}