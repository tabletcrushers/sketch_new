﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jump : MonoBehaviour
{
    public Hero _hero;
    public Rigidbody2D _body;

    public void RegularJump()
    {
        _hero.stateMachine.SwitchState("HeroStateJump");
    }

    public void ShortJump()
    {
        _hero.stateMachine.SwitchState("HeroStateJump");


        StartCoroutine(TimeUtils.Delay(.2f, () => { _hero.GetComponent<HeroStateInAir>()._forceDown = true; _hero.SetAnimTrigger("Trick");}));
        _hero.soundManager.PlayHeroJumpWoop();
    }
}
