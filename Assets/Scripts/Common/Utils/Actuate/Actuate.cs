﻿using UnityEngine;
using System.Collections;
using System;

public static class Actuate 
{
    private static MonoBehaviour coroutines;
    private static GameObject coroutinesObject;

    public static IEnumerator Tween (Action<float> action, float startpos, float endpos, float seconds, Func<float, float> Easing, Action complete = null) 
    {
        float t = .0f;

        while (t <= 1.0) 
        {
            t += Time.deltaTime / seconds;
            float step = Easing(t);
            action(Mathf.Lerp(startpos, endpos, step));
            yield return new WaitForSeconds(0);
        }
        complete?.Invoke();
    }
    public static IEnumerator TweenV3 (Action<Vector3> action, Vector3 startpos, Vector3 endpos, float seconds, Func<float, float> Easing, Action complete = null) 
    {
        float t = .0f;

        while (t <= 1.0) 
        {
            t += Time.deltaTime / seconds;
            float step = Easing(t);
            action(Vector3.Lerp(startpos, endpos, step));
            yield return new WaitForSeconds(0);
        }
        complete?.Invoke();
    }

    public static IEnumerator Oscillation(Action<float> action, float amplitude = 1f, float decay = 1f, float frequence = 4f, uint stepsLim = 480, float addValue = 0)
    {
        uint steps = 0;
        float startTime = Time.time;
        float t;

        for(;;)
        {
            t = Time.time - startTime;
            action(amplitude * Mathf.Sin(t * frequence * Mathf.PI * 2) / Mathf.Exp(t * decay) + addValue);
            steps++;
            if (steps > stepsLim)
            {
                break;
            }
            yield return new WaitForSeconds(0);
        }

        action(0);
    }

    public static IEnumerator TweenPosition(Transform target, Vector3 endpos, float seconds, Func<float, float> Easing, Action complete = null)
    {
        float t = .0f;

        Vector3 curentPosition;
        Vector3 startpos = target.position;

        void action(Vector3 val) => curentPosition = val;

        while (t <= 1.0)
        {
            t += Time.deltaTime / seconds;
            float step = Easing(t);
            action(Vector3.Lerp(startpos, endpos, step));

            target.position = curentPosition;

            yield return new WaitForSeconds(0);
        }

        target.position = endpos;
        complete?.Invoke();
    }

    public static void TweenPos(Transform target, Vector3 endpos, float seconds, Func<float, float> Easing, Action complete = null)
    {
        IEnumerator cr = TweenPosition(target, endpos, seconds, Easing, complete);

        GetCoroutines().StartCoroutine(cr);
    }

    private static MonoBehaviour GetCoroutines()
    {
        if (coroutinesObject == null)
        {
            coroutinesObject = new GameObject();
            coroutines = coroutinesObject.AddComponent<MonoBehaviour>();
        }

        return coroutines;
    }
}