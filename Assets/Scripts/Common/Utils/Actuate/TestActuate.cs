﻿using System.Collections;
using UnityEngine;

public class TestActuate : MonoBehaviour
{

    void Start()
    {
        Transform t = transform.GetChild(0);

        EasyTwinner.Instance.Delay(t, 1, () => EasyTwinner.Instance.StopAllOfCertain(t));


        EasyTwinner.Instance.TweenPosition(t, new Vector3(0, 4, 0), 4, Easing.Linear);
        EasyTwinner.Instance.TweenRotation(t, new Vector3(0, 0, 270), 4, Easing.Linear);
    }
}
