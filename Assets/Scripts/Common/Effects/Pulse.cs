﻿using UnityEngine;

public class Pulse : MonoBehaviour
{
    public float scaleFrom = 1;
    public float scaleTo = 1.4f;
    public float velocityExpand = .04f;
    public float velocityBack = .01f;

    private bool isExpanding = true;

    void Update()
    {
        if (isExpanding)
        {
            float vel = velocityExpand * Time.deltaTime;
            gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x + vel, gameObject.transform.localScale.y + vel, 1);

            if (gameObject.transform.localScale.x >= scaleTo) isExpanding = false;
        }
        else
        {
            float vel = velocityBack * Time.deltaTime;
            gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x - vel, gameObject.transform.localScale.y - vel, 1);

            if (gameObject.transform.localScale.x <= scaleFrom) isExpanding = true;
        }
    }
}
