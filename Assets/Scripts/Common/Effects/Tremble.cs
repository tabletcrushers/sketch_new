﻿using UnityEngine;

public class Tremble : MonoBehaviour
{
    public float trembleDeltaMax = 1.2f;
    public float invokeRate = 0.04f;

    private bool moveBack = false; 
    private float trembleDeltaX;
    private float trembleDeltaY;

    private void Start()
    {
        InvokeRepeating("TrembleEffect", 0, invokeRate);
    }

    private void TrembleEffect()
    {
        if(moveBack)
        {
            gameObject.transform.Translate(new Vector3(-trembleDeltaX, -trembleDeltaY, 0));
            moveBack = false;
            return;
        }

        trembleDeltaX = Random.value * trembleDeltaMax;
        trembleDeltaY = Random.value * trembleDeltaMax;

        gameObject.transform.Translate(new Vector3(trembleDeltaX, trembleDeltaY, 0));

        moveBack = true;
    }
}
