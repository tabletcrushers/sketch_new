﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;

[RequireComponent(typeof(Collider2D))]
public class TriggerChecker : MonoBehaviour
{
    public UnityAction<Collider2D> enterActions;
    public UnityAction<Collider2D> exitActions;
	public List<Collider2D> collidersIn = new List<Collider2D>();

	void OnTriggerEnter2D(Collider2D other)
	{
        if (other.gameObject.layer == 17) return;

        collidersIn.Add(other);
        if (enterActions != null) enterActions.Invoke(other);
	}

	void OnTriggerExit2D(Collider2D other)
	{
        if(!gameObject.activeInHierarchy) return;
        if (other.gameObject.layer == 17) return;
        collidersIn.Remove(other);
        if (exitActions != null) exitActions.Invoke(other);
	}
}