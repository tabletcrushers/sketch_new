﻿using UnityEngine;

public class AddForce : MonoBehaviour
{
    private Rigidbody2D _body;

    void Start()
    {
        _body = GetComponent<Rigidbody2D>();
    }

    public void Apply()
    {
        _body.AddForce(new Vector2(-700, 0));
    }
}
